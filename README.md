# Javier Morales - Data Analysis Portfolio

## Introduction

Hi, I'm Javier! I am currently a Development Coordinator at a non-profit institution, seeking to leverage my data analytical skills to improve corporate performance as a data analyst.

My skills include SQL (Data Cleanup, Data Exploration, Data Analysis, Transform, Load), Excel (Pivot Tables, Dashboards, Modeling, VLOOKUP), Python (pandsas, DataFrames, Seaborn, Matplotlib) and Tableau (Dashboards).

I have been honing my data analytics skills within my role and through external projects. My data projects in my current role include creating and managing our first Crowdfunding database, creating a SQL concatenation stored procedure to save 3 hours/week by automating customer concatenation for mailing data, and saving 2 hours/week automating various weekly reports through Excel macros and VLOOKUPS.
My other experience includes 4+ years of managing concurrent projects with competing deadlines, growing a Crowdfunding program to a $380,000 per year revenue core function, and presenting to Senior level colleagues with technical and non-technical backgrounds.

<br>
I will be storing my various external data analytics portfolio projects here to showcase my SQL skills, share my projects, and track my growth.

## Table of Contents
- [Introduction](#introduction)
- [Projects](#projects)
	+ [Danny's Diner: SQL and Analysis](#dannys-diner-sql-and-analysis)
	+ [Data Cleanup in SQL](#data-cleanup-in-sql)
	+ [COVID Data Analysis](#covid-project)
		+ [Tableau COVID Dashboard](#tableau-covid-dashboard)
	+ [Google Analytics Data Studio Dashboard](#google-analytics-data-studio-dashboard)

## Projects

In this section, I will list and track my Data Analysis projects and explain the skills that are showcased.

<br>

### Danny's Diner: SQL and Analysis

- **Code and Analysis:** [`Danny's Diner: Analysis`](https://gitlab.com/javier-morales-portfolio/portfolio/-/blob/f0b6da204c22e06e2bd8a8a65e963a4a08b0ab59/Case%20Study:%20Danny's%20Diner/Analysis.md)
- **Files:** [`File Directory`](https://gitlab.com/javier-morales-portfolio/portfolio/-/tree/f0b6da204c22e06e2bd8a8a65e963a4a08b0ab59/Case%20Study%3A%20Danny's%20Diner)
- **Summary:** I solve and analyze a case study by [Data with Danny](https://8weeksqlchallenge.com/case-study-1/). The purpose of this challenge is to, utilizing a limited sample dataset, write SQL queries to answer Danny's questions and provide him a product he can use on his full dataset.
- **Language(s):** SQL, Markdown
- **Skills:** Scalable SQL Queries, Analysis of Customer Behavior, Aggregate Functions, Nested CTEs, Ranking Window Functions, Rewards System Implementation

<br>

### Data Cleanup in SQL
(50,000 records)

- **Code:** [`Data Cleaning SQL Queries.sql`](https://gitlab.com/javier-morales-portfolio/portfolio/-/blob/f0b6da204c22e06e2bd8a8a65e963a4a08b0ab59/Data%20Cleaning%20in%20SQL/Data_Cleaning_SQL_Queries.sql)
- **Files:** [`File Directory`](https://gitlab.com/javier-morales-portfolio/portfolio/-/tree/f0b6da204c22e06e2bd8a8a65e963a4a08b0ab59/Data%20Cleaning%20in%20SQL)
- **Summary:** Utilizing a Nashville housing dataset, I utilize SQL queries to clean a few columns. I showcase various complex SQL skills which I highlighted below.
- **Language(s):** SQL
- **Skills:** Self Joins, Nested CTEs, Filling NULL Values Within Column Using Other Columns, Loading Data Corrections, Substrings, CHARINDEX, Splitting Clean Data Into New Columns, PARSENAME, Replacing Y and N Values with Yes/No in Field, Removing Duplicates, Backing Up Deleted Data to New Table for Client Reference

<br>

### COVID Project
(80,000 records)

- **Code:** [`COVID Project Data Exploration.sql`](https://gitlab.com/javier-morales-portfolio/portfolio/-/blob/f0b6da204c22e06e2bd8a8a65e963a4a08b0ab59/COVID%20Project/COVID_Project_Data_Exploration.sql)
- **Files:** [`File Directory`](https://gitlab.com/javier-morales-portfolio/portfolio/-/tree/f0b6da204c22e06e2bd8a8a65e963a4a08b0ab59/COVID%20Project)
- **Summary:** Utilizing a date-limited COVID data set from OurWorldInData.org, I wrote various SQL queries for exploratory data analysis. I showcase various essential SQL skills which I highlighted below.
- **Language(s):** SQL
- **Skills:** Joins, CTE's, Temp Tables, Subqueries, Window Functions, Aggregate Functions, Creating Views, Converting Data Types

<br>

### Tableau COVID Dashboard

- **Tableau Public:** [`Dashboard`](https://public.tableau.com/shared/4GFZB6R32?:display_count=n&:origin=viz_share_link)
- **Queries Used:** [`SQL Queries`](https://gitlab.com/javier-morales-portfolio/portfolio/-/blob/f0b6da204c22e06e2bd8a8a65e963a4a08b0ab59/Tableau%20COVID%20Dashboard/Tableau_Project_Queries.sql)
- **Summary:** Tableau Public Dashboard created as a high-level visualization of data gathered from SQL COVID Project. Includes high-level summary numbers, a heat-map, breakdown by Continent, and a line graph plotting cases over time by country.
- **Technology:** Tableau, Tableau Public, SQL
- **Skills:** Designing a Dashboard, Data Visualization, Forecasting, Geographical Data Analysis

<br>

### Google Analytics Data Studio Dashboard

- **Data Studio:** [`Dashboard`](https://datastudio.google.com/reporting/7f2630e8-6b6c-40f0-ad6a-7ec2535df701)
- **Summary:** Google Data Studio Dashboard created as a high-level visualization of Google Analytics data for a merchandise store. Includes high-level summary, adjustable time periods and trends, line graphs plotting trends over time, acquisition by channel overview, campaign performance, and drilldown map data.
- **Technology:** Google Data Studio, Google Analytics 4 (GA4)
- **Skills:** Designing a Dashboard, Data Visualization, Drilldown, Traffic Analytics, Campaign Performance
